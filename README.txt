Author: Darren Silke
Date: 15 March 2015

Name: Assignment 2 - VolImage - A Volumetric Image Manipulation Tool Using Raw Pointers

Description: This software builds a volumetric image tool, which can load a "stack" of 2D images and manipulate them as a group. Volumetric images are produced by many instruments, including radio astronomy telescopes and MRI machines used in medical imaging. 

Instructions:

1. Open terminal BASH.
2. Type 'make' to compile and link all C++ source files.
3. To run the software, see HOW TO RUN below.
4. Follow the instructions that appear on the screen (if any).
5. Evaluate output.
6. To remove the 'volimage' executable and the .o files, type 'make clean'.

HOW TO RUN:

To run the application, make sure that any files needed by the application are in the same directory as the application. In this case, ensure that the MRI.dat and all MRI.raw files are in the same directory as the application.

To invoke the application on the terminal, the following format is required, as the application relies on command line arguments:

volimage <imageBase> [-d i j output_file_name] [-x i output_file_name] [-g i output_file_name]

where 'volimage' is the name of the executable, <imageBase> is the prefix for the file sequence ("MRI" in this case). The angle brackets are a convention to show the argument must be provided. There are then three optional command line arguments (indicated by the [] brackets):

1. -d i j output_file_name: Compute a difference map between images i and j, and write this out to file (Note: files are indexed from 0).

2. -x i output_file_name: Extract and write the slice with number i and write this out to file (Note: files are indexed from 0).

3. -g i output_file_name: Extract an image along row i of the volume, across all slices, and write this out to file (Note: files are indexed from 0).

BELOW ARE SOME EXAMPLES OF INVOKING THE APPLICATION USING THE MRI.dat AND MRI.raw FILES:

1. volimage MRI
2. volimage MRI -d 0 1 Result
3. volimage MRI -x 0 Result
4. volimage MRI -g 0 Result

Please refer to the assignment instructions for more information.

List Of Files:

1. README.txt - Information file.
2. Makefile - Used to compile the program.
3. VolImage.h - Declares the functions to be implemented in the program by the VolImage.cpp file.
4. VolImage.cpp - Implements the functions declared in the VolImage.h file.
5. Main.cpp - Driver file where the main function lies. This file makes use of both the VolImage.h and VolImage.cpp files.
6. viewer.py - Used to view a .raw file (included for convenience sake).
7. diffmap.py - Used to generate and view a difference map file generated from two .raw files (included for convenience sake).
8. MRI.dat - Header file which specifies the dimensions for all .raw files in the sequence. It also specifies the number of .raw files in the sequence (included for convenience sake).
9. MRI0.raw - MRI122.raw - Image files used by application (included for convenience sake). 
10. MRI_originals_png folder - Contains 123 original MRI images (included for interest sake).

TAKE NOTE OF THE .git FOLDER WHICH CONTAINS ALL INFORMATION RELATING TO THE USE OF GIT FOR A LOCAL REPOSITORY AS REQUIRED BY THIS COURSE.
