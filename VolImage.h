/* 
 * File:   VolImage.h
 * Author: Darren Silke
 *
 * Created on 06 March 2015, 9:30 AM
 */

#ifndef VOLIMAGE_H
#define	VOLIMAGE_H

#include <vector>
#include <string>

using std::vector;
using std::string;

namespace SLKDAR001
{
    class VolImage
    {
    // Private members.
    private:
        // Width and height of image stack.
        int width, height; 
        // Data for each slice, in order.
        vector<unsigned char**> slices; 
        
    // Public members.
    public: 
        // Default constructor - define in .cpp file.
        VolImage(); 
        // Destructor - define in .cpp file.
        ~VolImage();   
        // Populate the object with images in stack and 
        // set member variables - define in .cpp file.
        bool readImages(string baseName);
        
        // Compute difference map and write out - define in .cpp file.
        void diffmap(int sliceI, int sliceJ, string output_prefix);
        
        // Extract slice sliceId and write to output - define in .cpp file.
        void extract(int sliceId, string output_prefix);
        
        // Number of bytes used to store image data bytes
        // and pointers (ignore vector<> container, dims etc).
        // Define in .cpp file.
        int volImageSize(void); 
        
        // This function extracts an image along row i of the volume, across all
        // slices, and writes this out to a .raw image file. 
        void bonus(int rowNumber, string output_prefix);
    };
}
#endif	/* VOLIMAGE_H */
