# Makefile for compiling C++ source code.
# Darren Silke (SLKDAR001), 2015

CC=g++ # The compiler name.
CCFLAGS=-std=c++11 # Flags passed to compiler.

# The normal build rules.

volimage: Main.o VolImage.o
	$(CC) $(CCFLAGS) Main.o VolImage.o -o volimage

Main.o: Main.cpp
	$(CC) $(CCFLAGS) Main.cpp -c
	
VolImage.o: VolImage.cpp VolImage.h
	$(CC) $(CCFLAGS) VolImage.cpp -c

# Clean rule.

clean:
	rm -f *.o volimage
