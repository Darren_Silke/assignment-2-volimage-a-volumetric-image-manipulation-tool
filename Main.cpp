/* 
 * File:   Main.cpp
 * Author: Darren Silke
 *
 * Created on 07 March 2015, 2:27 PM
 */

#include <string>
#include <cstdlib>
#include <iostream>

#include "VolImage.h"

using std::string;
using std::cout;
using std::cerr;
using std::endl;
using SLKDAR001::VolImage;

/*
 * Main function.
 */
int main(int argc, char* argv[])
{        
    VolImage volImage;
    
    // If none of the optional arguments are provided...
    if(argc == 2)
    {  
        string baseName = string(argv[1]);  
        bool operationSuccessful = volImage.readImages(baseName);
        if(operationSuccessful == false)
        {
            cerr << "The operation to read the images has failed.\n";
            exit(1);
        }
        else
        {
            int numberOfBytesRequired = volImage.volImageSize();
            cout << "Number of bytes required: " << numberOfBytesRequired << endl; 
        }
    }
    // If the optional arguments to generate a difference map image are 
    // provided...
    else if(argc == 6)
    {
        // Checks if the flag is indeed "-d".
        if(string(argv[2]) == "-d")
        {
            string baseName = string(argv[1]);
            bool operationSuccessful = volImage.readImages(baseName);
            if(operationSuccessful == false)
            {
                cerr << "The operation to read the images has failed.\n";
                exit(1);
            }
            else
            {
                int numberOfBytesRequired = volImage.volImageSize();
                cout << "Number of bytes required: " << numberOfBytesRequired << endl;
                volImage.diffmap(atoi(argv[3]), atoi(argv[4]), string(argv[5]));
                cout << "A diffmap file has been successfully generated.\n"
                     << "The diffmap file is called: " << string(argv[5]) << ".raw\n"
                     << "A new header file has been successfully generated.\n"
                     << "The new header file is called: " << string(argv[5]) << ".dat\n";
            }
        }
        // Some other flag was given, print error message.
        else
        {
            cerr << argv[2] << " is not a valid flag.\n";
            exit(1);
        }
    }
    // If the optional arguments to either extract an image using the image id, 
    // or to generate an image from the image stack along a particular row are 
    // provided... 
    // Need to check whether the flag provided is "-x" or "-g".
    else if(argc == 5)
    {
        // If the flag provided is "-x"...
        if(string(argv[2]) == "-x")
        {
            string baseName = string(argv[1]);
            bool operationSuccessful = volImage.readImages(baseName);
            if(operationSuccessful == false)
            {
                cerr << "The operation to read the images has failed.\n";
                exit(1);
            }
            else
            {
                int numberOfBytesRequired = volImage.volImageSize();
                cout << "Number of bytes required: " << numberOfBytesRequired << endl;
                volImage.extract(atoi(argv[3]), string(argv[4]));
                cout << "Image number " << atoi(argv[3]) << " has been successfully extracted.\n"
                     << "A new image file has been created.\n"
                     << "The new image file is called: " << string(argv[4]) << ".raw\n"
                     << "A new header file has been successfully generated.\n"
                     << "The new header file is called: " << string(argv[4]) << ".dat\n";            
            }
        }
        // If the flag provided is "-g".
        else if(string(argv[2]) == "-g")
        {
            string baseName = string(argv[1]);
            bool operationSuccessful = volImage.readImages(baseName);
            if(operationSuccessful == false)
            {
                cerr << "The operation to read the images has failed.\n";
                exit(1);
            }
            else
            {
                int numberOfBytesRequired = volImage.volImageSize();
                cout << "Number of bytes required: " << numberOfBytesRequired << endl;
                volImage.bonus(atoi(argv[3]), string(argv[4]));
                cout << "Row number " << atoi(argv[3]) << " has been successfully extracted along all images in the volume.\n"
                     << "A new image file has been created.\n"
                     << "The new image file is called: " << string(argv[4]) << ".raw\n"
                     << "A new header file has been successfully generated.\n"
                     << "The new header file is called: " << string(argv[4]) << ".dat\n";             
            }
        }
        // Some other flag was given, print error message.
        else
        {
            cerr << argv[2] << " is not a valid flag.\n";
            exit(1);
        }
    }
    // The command line arguments were not specified in the required format, and 
    // are thus invalid.
    // Print error message.
    else
    {
        cout << "Invalid command line arguments.\n"
             << "The command line arguments should be specified in the following format:\n"
             << "volimage <imageBase> [-d i j output_file_name] [-x i output_file_name] [-g i output_file_name]\n"
             << "Please refer to the assignment instructions for more information.\n";
        exit(1);
    }
    return 0;
}
