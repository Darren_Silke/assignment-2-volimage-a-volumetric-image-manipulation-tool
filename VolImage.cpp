/* 
 * File:   VolImage.cpp
 * Author: Darren Silke
 *
 * Created on 07 March 2015, 2:22 PM
 */

#include <vector>
#include <string>
#include <fstream>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <sstream>

#include "VolImage.h"

using std::vector;
using std::string;
using std::ifstream;
using std::ofstream;
using std::cerr;
using std::cout;
using std::endl;
using std::ws;
using std::ios;
using std::stringstream;

namespace SLKDAR001
{    
    VolImage::VolImage() : width(0), height(0)
    {
        /*
         * Deliberately left empty.
         */
    }
    VolImage::~VolImage()
    {
        for(int sliceIndex = 0;sliceIndex < slices.size(); sliceIndex++)
        {
            for(int row = 0; row < height; row++)
            {
                // Destroy the array of columns.
                delete[] slices[sliceIndex][row];
            }
            // Destroy the array of rows.
            delete[] slices[sliceIndex];
        }
    }
    bool VolImage::readImages(string baseName)
    {
        bool operationSuccessful = false;
        
        // Open file in text mode.
        ifstream inStream((baseName + ".dat").c_str());
        if(inStream.fail())
        {
            cerr << "Input file " + baseName + ".dat" + " opening failed.\n";
            return operationSuccessful;
        }
        
        int widthOfImages, heightOfImages, numberOfImages;
        inStream >> widthOfImages >> ws;
        inStream >> heightOfImages >> ws;
        inStream >> numberOfImages >> ws;
        inStream.close();   
        
        width = widthOfImages;
        height = heightOfImages;
                        
        int sliceSize = width * height;       
        // Each .raw image file is first read into a 1D array, before being 
        // converted into an appropriate 2D array as per the assignment 
        // instructions.
        unsigned char* rawSlice = new unsigned char[sliceSize]; 
        stringstream ss;
        string imageIndexStr;
        for(int imageIndex = 0; imageIndex < numberOfImages; imageIndex++)
        {
            ss.clear();
            ss.str("");
            ss << imageIndex;
            imageIndexStr = ss.str();       
            // Open file in binary mode.
            inStream.open((baseName + imageIndexStr + ".raw").c_str(), ios::binary);
            if(inStream.fail())
            {
                cerr << "Input file " + baseName + imageIndexStr + ".raw" + " opening failed.\n";
                return operationSuccessful;
            }       
            // While there is still stuff to read...
            while(!inStream.eof())
            {
                inStream.read(reinterpret_cast<char*>(rawSlice), sliceSize);
            }
            inStream.close();  
            
            // Creation of 2D array to store an image.
            unsigned char** slice = new unsigned char*[height];       
            for(int row = 0; row < height; row++)
            {
                slice[row] = new unsigned char[width];
            }     
            int sliceIndex = 0;   
            for(int row = 0; row < height; row++)
            {
                for(int column = 0; column < width; column++)
                {
                    slice[row][column] = rawSlice[sliceIndex];
                    sliceIndex++;
                }
            }         
            slices.push_back(slice);
        }
        // Delete the rawSlice array, that was used temporally to store each 
        // image.
        delete[] rawSlice;
        // Since no errors were thrown, the function completed successfully.
        operationSuccessful = true;
        return operationSuccessful;
    }
    void VolImage::diffmap(int sliceI, int sliceJ, string output_prefix)
    { 
        // Open file in text mode.
        ofstream outStream((output_prefix + ".dat").c_str());
        outStream << width << " " << height << " " << 1;
        outStream.close();
        
        // Creation of 2D array to store the new image that will be created.
        unsigned char** diffmap = new unsigned char*[height];       
        for(int i = 0; i < height; i++)
        {
            diffmap[i] = new unsigned char[width];
        }  
        
        // Generate the difference map image.
        for(int row = 0; row < height; row++)
        {
            for(int column = 0; column < width; column++)
            {
                diffmap[row][column] = (unsigned char)(abs((float)slices[sliceI][row][column] - (float)slices[sliceJ][row][column])/2);
            }
        }   
        
        // Open file in binary mode.
        outStream.open((output_prefix + ".raw").c_str(), ios::binary);        
        for(int i = 0; i < height; i++)
        {         
            outStream.write(reinterpret_cast<char*>(diffmap[i]), width);  
        }
        outStream.close();  
        
        // Destroy the 2D array that was used to store the difference map, as it
        // has been safely written to a file.
        for(int i = 0; i < height; i++)
        {
            // Destroy the array of columns.
            delete[] diffmap[i];
        }
        // Destroy the array of rows.
        delete[] diffmap;      
    }
    void VolImage::extract(int sliceId, string output_prefix)
    {
        // Open file in text mode.
        ofstream outStream((output_prefix + ".dat").c_str());
        outStream << width << " " << height << " " << 1;
        outStream.close();
        
        // Open file in binary mode.
        outStream.open((output_prefix + ".raw").c_str(), ios::binary);
        for(int row = 0; row < height; row++)
        {
            outStream.write(reinterpret_cast<char*>(slices[sliceId][row]), width);
        }
        outStream.close();
    }
    int VolImage::volImageSize()
    {        
        int numberOfBytesRequired = 0;
        int sizeOfPointer = sizeof(unsigned char*);
        numberOfBytesRequired = (height * sizeOfPointer) + sizeOfPointer;
        numberOfBytesRequired += height * width;
        numberOfBytesRequired *= slices.size();
        
        cout << "Number of images: " << slices.size() << endl;      
        return numberOfBytesRequired;
    }
    void VolImage::bonus(int rowNumber, string output_prefix)
    {
        // The height of the new image that will be created is equal to the 
        // number of images in the volume.
        height = slices.size();
        
        // Open file in text mode.
        ofstream outStream((output_prefix + ".dat").c_str());
        outStream << width << " " << height << " " << 1;
        outStream.close();
        
        // Creation of 2D array to store the new image that will be created.
        unsigned char** newImage = new unsigned char*[height];       
        for(int row = 0; row < height; row++)
        {
            newImage[row] = new unsigned char[width];
        }
    
        // Generate the new image.
        int sliceIndex = 0;
        for(int row = 0; row < height; row++)
        {
            for(int column = 0; column < width; column++)
            {
                newImage[row][column] = slices[sliceIndex][rowNumber][column];
            }
            sliceIndex++;
        }
            
        // Open file in binary mode.
        outStream.open((output_prefix + ".raw").c_str(), ios::binary);        
        for(int row = 0; row < height; row++)
        {         
            outStream.write(reinterpret_cast<char*>(newImage[row]), width);  
        }
        outStream.close();  
                
        // Destroy the 2D array that was used to store the new image, as it has 
        // been safely written to a file.
        for(int row = 0; row < height; row++)
        {            
            // Destroy the array of columns.
            delete[] newImage[row];
        }      
        // Destroy the array of rows.
        delete[] newImage;  
    }
}
